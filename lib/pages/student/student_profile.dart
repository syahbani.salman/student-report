import 'package:flutter/material.dart';
import 'package:tutorialapp/models/student.dart';

class StudentProfile extends StatelessWidget {
  const StudentProfile({Key? key}) : super(key: key);

  static const String routeName = "/student_profile";

  @override
  Widget build(BuildContext context) {
    Student student = ModalRoute.of(context)!.settings.arguments as Student;

    return Scaffold(
      appBar: AppBar(
        title: Text("${student.name} Profile"),
      ),
      body: Container(
        child: Column(),
      ),
    );
  }
}
