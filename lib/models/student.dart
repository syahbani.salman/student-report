//import "package:uuid/uuid.dart";
import 'package:username_generator/username_generator.dart';

class Student {
  final String grade;
  final String name;
  final String gradeCode;

  const Student(this.grade, this.name, this.gradeCode);
}

Iterable<Student> StudentsList() sync* {
  var username = UsernameGenerator()..separator = ' ';

  for (int i = 0; i < 50; i++) {
    yield Student("XII", username.generateRandom(), "A");
  }
}
