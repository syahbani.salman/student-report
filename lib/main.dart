import 'package:flutter/material.dart';
import 'package:tutorialapp/models/student.dart';
import 'package:tutorialapp/pages/student/student_profile.dart';
import 'package:tutorialapp/routes/routes.dart';

void main() {
  runApp(MainApp);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String routeName = "/home";
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Student Report"),
      ),
      body: const Students(),
    );
  }
}

class Students extends StatelessWidget {
  const Students({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            for (var student in StudentsList()) _listBuild(context, student)
          ],
        ));
  }

  Widget _listBuild(BuildContext context, Student student) {
    return Material(
      child: ListTile(
        title: Text(
          student.name,
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        subtitle: Text("Kelas ${student.grade} ${student.gradeCode}",
            style: TextStyle(
              fontWeight: FontWeight.w300,
              color: Color.fromARGB(80, 0, 0, 0),
            )),
        onTap: () {
          Navigator.pushNamed(context, StudentProfile.routeName,
              arguments: student);
        },
      ),
    );
  }
}
